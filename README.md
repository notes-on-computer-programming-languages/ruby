# ruby

# Books
* [*The Well-Grounded Rubyist*
  ](https://www.worldcat.org/search?q=ti%3A+Well-grounded+Rubyist)
  2019-03 (Third Edition) David A. Black, Joseph Leo III
  * Chapter 16. Ruby and functional programming
* *The Ruby programming language*
  2008 David Flanagan, Yukihiro Matsumoto
  * Chapter 6. Methods, Procs, Lambdas, and Closures
    * ...
    * Functional Programming

# Unofficial documentation
## Functional programming
* [*Functional Programming in Ruby — State*
  ](https://medium.com/@baweaver/functional-programming-in-ruby-state-5e55d40b4e67)
  2018-05 Brandon Weaver
* [*Functional Programming In Ruby (Complete Guide)*
  ](https://www.rubyguides.com/2018/01/functional-programming-ruby/)
  2018-01 Jesus Castello
* [*Functional programming in Ruby*
  ](https://womanonrails.com/functional-programming-ruby) Short introduction to blocks, procs, lambdas and closures
  (2022) Agnieszka Małaszkiewicz

## Pattern matching (Ruby 2.7)
... 

# Tools
* [Ruby Version Manager (RVM)](https://rvm.io/)

# Ruby implementations
* https://repology.org/project/ruby/versions
![Ruby at Debian](https://qa.debian.org/cgi-bin/popcon-png?packages=ruby%20ruby1.8%20ruby1.9.3%20ruby2.1%20ruby2.3%20ruby2.5%20ruby2.7%20ruby3.0&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%25Y-%25m&beenhere=1)

* https://repology.org/project/jruby/versions
* https://repology.org/project/mruby/versions
![JRuby and mruby at Debian](https://qa.debian.org/cgi-bin/popcon-png?packages=jruby%20mruby&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%25Y-%25m&beenhere=1)

* Opal is a Ruby to JavaScript compiler. It is source-to-source, making it fast as a runtime. Opal includes a compiler (which can be run in any browser), a corelib and runtime implementation. The corelib/runtime is also very small. https://rubygems.org/gems/opal

# Relationship with other languages
* [*Children of Ruby: The different worlds of Elixir and Crystal*
  ](https://conferences.oreilly.com/oscon/oscon-or-2019/public/schedule/detail/75715.html)
  2019-07 Simon St.Laurent

# What can be done with ruby
## (Monadic) Parser Combinators
* [*Parser Combinators in Ruby*
  ](https://hmac.dev/posts/2019-05-19-ruby-parser-combinators.html)
  2019-05 Harry Maclean
* https://github.com/cconstable/rupac A monadic parser combinator library for Ruby
* ruby Monadic parser combinators

## Do notation
* https://dry-rb.org/gems/dry-monads/master/do-notation/

## Fiber
* [ruby fiber](https://google.com/search?q=ruby+fiber)
* [*Fiber*](https://ruby-doc.org/core-2.7.0/Fiber.html)
* [*What Everyone Should Know About Fibers in Ruby*
  ](https://www.rubyguides.com/2019/11/what-are-fibers-in-ruby/)
  2019-11 Jesus Castello
* [*Ruby 3.0 and the new FiberScheduler interface*
  ](http://www.wjwh.eu/posts/2020-12-28-ruby-fiber-scheduler-c-extension.html)
  2020-12 wjwh

## Concurrent programming
![ruby-concurrent popcon](https://qa.debian.org/cgi-bin/popcon-png?packages=ruby-concurrent%20ruby-concurrent-ext%20ruby-async&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%25Y-%25m&beenhere=1)
* https://tracker.debian.org/pkg/ruby-concurrent

### For Ruby 3
* https://github.com/ko1/ractor-tvar
* ruby 3 ractor tvar

# Libraries (gem)
## dry-monads
* [*Monad laws in Ruby*
  ](https://www.morozov.is/2018/09/08/monad-laws-in-ruby.html)
  2018 Igor Morozov
* [*Functional Ruby with `dry-monad`s*
  ](https://humanreadablemag.com/issues/0/articles/functional-ruby-with-dry-monads/)
  Michael Kohl

## Web

![Ruby web at Debian](https://qa.debian.org/cgi-bin/popcon-png?packages=rails%20ruby-rails%20ruby-sinatra&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%25Y-%25m&beenhere=1)


